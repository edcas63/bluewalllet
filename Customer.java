package Package;

public class Customer extends Person {

	private String idCustomer;
	private double avarageBalance;
	private int numberOfTransfers;
	private int numberOfWithDrawals;

	private String[] cuentas;
	private String[] inversiones;

	private String kindOfCustomer;
	private boolean isDelinquent;
	private boolean isDangerous;
	
	public String getIdCustomer() {
		return idCustomer;
	}
	public void setIdCustomer(String idCustomer) {
		this.idCustomer = idCustomer;
	}
	public double getAvarageBalance() {
		return avarageBalance;
	}
	public void setAvarageBalance(double avarageBalance) {
		this.avarageBalance = avarageBalance;
	}
	public int getNumberOfTransfers() {
		return numberOfTransfers;
	}
	public void setNumberOfTransfers(int numberOfTransfers) {
		this.numberOfTransfers = numberOfTransfers;
	}
	public int getNumberOfWithDrawals() {
		return numberOfWithDrawals;
	}
	public void setNumberOfWithDrawals(int numberOfWithDrawals) {
		this.numberOfWithDrawals = numberOfWithDrawals;
	}





}
