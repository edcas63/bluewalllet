package Package;

public interface ATM {
	
	public void withDraw(int amount);
	public void getPIN();
	public void getBalance(String idCustomer);
	public void rejectCard();
	public void reEnterPIN();
	public void enterPIN();
	public boolean verifyAccount();
	public void showMenu();
	public void updateBalanceCard(double newBalance);
	public void aceptCredit();
	public void exit();
	

}
