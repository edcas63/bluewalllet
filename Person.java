package Package;

public  class Person {

    private int age;
    private boolean gender;
    private String name;
	private String apMaterno;
	private String apPaterno;
    private String address;


	private String phone;
	private String email;


    private int birthDate;

    private String idINE;
    private boolean isMexican;
    private boolean isALegalAdult;
	
    public int getAge(){
        return age;
    }

    public boolean getGender(){
        return gender;
    }

    public String getName(){
        return name;
    }

    public String getAddress(){
        return address;
    }

    public int setAge(){
        return age;
    }

    public boolean setGender(){
        return gender;
    }

    public String setName(){
        return name;
    }

    public String setAddress(){
        return address;
    }

}
